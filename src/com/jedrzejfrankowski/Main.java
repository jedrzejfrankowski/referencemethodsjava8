package com.jedrzejfrankowski;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Main {

    public static void main(String[] args) {
        referenceToConstructorExample();
    }

    private static void referenceToStaticMethodExample() {
        List<Integer> integers = new ArrayList<>();
        for(int i = 0; i<1000; i++) {
            integers.add(i);
        }
        System.out.println(ReferenceToStaticMethodExample.findAllPrimes(integers,ReferenceToStaticMethodExample::isPrime));
    }
    private static void referenceToConstructorExample(){
        Consumer c = System.out::println;
        ReferenceToConstructorExample referenceToConstructorExample = String::new;
        c.accept(referenceToConstructorExample.strFunc(new char[]{'a', 'b', 'c'}));

    }
}
