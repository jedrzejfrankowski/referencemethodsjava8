package com.jedrzejfrankowski;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by jfrankowski on 07.02.17.
 */
public class ReferenceToStaticMethodExample {
    public static boolean isPrime(Integer n) {
        if(n==1) {
            return false;
        }
        for(int i=2;i<n;i++) {
            if(n%i==0) return false;
        }
        return true;
    }
    public static List findAllPrimes(List<Integer> integers, Predicate<Integer> predicate) {
        return integers.stream().filter(p->predicate.test(p)).collect(Collectors.toList());
    }
}
